var userRouter = require("express").Router();

var UserController = require('../controllers/user.controller');
var jwt = require('jsonwebtoken');
const passport = require('passport');
var passportConfig = require('../middleware/passport'); // bắt buộc

userRouter.get("/getAll", (req, res) => UserController.getAll(req, res));
userRouter.get("/getOne", (req, res) => UserController.getOne(req, res));
userRouter.post("/register", (req, res) => UserController.register(req, res));
userRouter.get("/:id/edit", (req, res) => UserController.edit(req, res));

module.exports = userRouter;
