const express = require("express");
var userRouter = require('./user.router')

const apiRoute = express();

apiRoute.use("/user", userRouter);

module.exports = apiRoute;
