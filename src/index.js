const express = require("express");
const helmet = require("helmet"); // security
const morgan = require("morgan");
const path = require('path');
const handlebars = require('express-handlebars');
const { connectDatabase } = require("./configs/db.config")
const apiRoute = require('./routers/router')
connectDatabase();
const dotenv = require("dotenv");
dotenv.config();

const app = express();

// HTTp logger
app.use(morgan('combined'));
//
console.log(path.join(__dirname, '/public'));
app.use(express.static(path.join(__dirname, '/public')));



app.use(helmet());
app.use(express.json());

const port = process.env.PORT || 1998;
app.use("/api", apiRoute);

//template engine
app.engine('hbs', handlebars({
    extname: ".hbs",
    helpers: {
        sum: (a, b) => a + b,
    }
}));
app.set('view engine', 'hbs');
console.log('hieu ', path.join(__dirname, 'resources/views'));
app.set('views', path.join(__dirname, 'resources/views'))



app.listen(port, () => {
    console.log("Successfully with port: " + port);
})